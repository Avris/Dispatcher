<?php
namespace Avris\Dispatcher;

abstract class Event
{
    /** @var bool */
    private $propagationStopped = false;

    abstract public function getName(): string;

    abstract public function setValue($value): self;

    abstract public function getValue();

    public function stopPropagation(): self
    {
        $this->propagationStopped = true;

        return $this;
    }

    public function isPropagationStopped(): bool
    {
        return $this->propagationStopped;
    }
}
