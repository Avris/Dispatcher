<?php
namespace Avris\Dispatcher;

class ListenerSet
{
    /** @var callable[][] */
    private $listeners = [];

    /** @var bool */
    private $sorted = false;

    public function add(callable $listener, int $priority = 0)
    {
        $this->listeners[$priority][] = $listener;
        $this->sorted = false;
    }

    public function get(): iterable
    {
        if (!$this->sorted) {
            krsort($this->listeners);
            $this->sorted = true;
        }

        foreach ($this->listeners as $priority => $listeners) {
            foreach ($listeners as $listener) {
                yield $listener;
            }
        }
    }

    /**
     * @codeCoverageIgnore
     */
    public function __debugInfo()
    {
        return $this->listeners;
    }
}
