## Avris Event Dispatcher ##

### Instalation

    composer require avris/dispatcher
    
### Event Definition

An event is an object that extends `Avris\Dispatcher\Event`.
It has to provide its name (used as an identifier for triggering it)
and a setter and getter for its value (for instance if `RequestEvent` is supposed to produce a `Response`, that response is the value).

For example:

    final class RequestEvent extends Event
    {
        /** @var RequestInterface */
        private $request;
    
        /** @var ResponseInterface */
        private $response;
    
        public function __construct(RequestInterface $request)
        {
            $this->request = $request;
        }
    
        public function getName(): string
        {
            return 'request';
        }
    
        public function getRequest(): RequestInterface
        {
            return $this->request;
        }
    
        public function getResponse(): ?ResponseInterface
        {
            return $this->response;
        }
    
        public function setResponse(ResponseInterface $response): self
        {
            $this->response = $response;
    
            return $this;
        }
    
        public function setValue($value): Event
        {
            $this->response = $value;
    
            return $this;
        }
    
        public function getValue()
        {
            return $this->response;
        }
    }

### Attaching listeners

    $dispatcher = new EventDispatcher();
    $dispatcher->attachListener('request', function (RequestEvent $event) {
        if (substr($event->getRequest()->getUrl(), 0, 6) === '/admin' && !is_admin()) {
            return new Response('Forbidden', 403);
        }
    });

Returning anything other than `null` will automatically set event's value
(you can also do it explicitly with `$event->setResponse($response)`).

As a last argument of `attachListener` you can specify the priority of the listener, for example:

    $dispatcher->attachListener('request', [$this, 'one']);
    $dispatcher->attachListener('request', [$this, 'two']);
    $dispatcher->attachListener('request', [$this, 'three'], -9);
    $dispatcher->attachListener('request', [$this, 'four'], 9);

The listeners will be triggered in order: `four`, `one`, `two`, `three`.

To stop propagation of an event, use `$event->stopPropagation()`.

### Triggering events

To trigger an event:

    $response = $dispatcher->trigger(new RequestEvent($request));

As you can see, the value of the event after executing the listeners will be returned for convenience
(again, you can explicitly use `$event->getResponse()`).

### Event subscribers

A subscriber is an object that defines event listeners in an elegant manner,
encapsulating possibly multiple listeners related to one service.

It needs to implement `Avris\Dispatcher\EventSubscriberInterface`:

    final class FooService implements EventSubscriberInterface
    {
        public function getSubscribedEvents(): iterable
        {
            yield 'request' => [$this, 'onRequest'];
            yield 'response:9' => [$this, 'onResponse']; // priority 9
        }
    }

To register them in the dispatcher:

    $fooService = new FooService();
    $this->dispatcher->registerSubscriber($fooService);

### Copyright ###

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
