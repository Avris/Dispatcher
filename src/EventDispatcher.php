<?php
namespace Avris\Dispatcher;

class EventDispatcher implements EventDispatcherInterface
{
    /** @var ListenerSet[] */
    private $events = [];

    public function registerSubscriber(EventSubscriberInterface $subscriber): EventDispatcherInterface
    {
        foreach ($subscriber->getSubscribedEvents() as $event => $listener) {
            list($eventName, $priority) = explode(':', $event) + [1 => 0];
            $this->attachListener($eventName, $listener, $priority);
        }

        return $this;
    }

    public function attachListener(string $event, callable $listener, int $priority = 0): EventDispatcherInterface
    {
        $set = $this->events[$event] ?? new ListenerSet();
        $set->add($listener, $priority);
        $this->events[$event] = $set;

        return $this;
    }

    public function clear(string $event): EventDispatcherInterface
    {
        unset($this->events[$event]);

        return $this;
    }

    public function trigger(Event $event)
    {
        $listeners = ($this->events[$event->getName()] ?? new ListenerSet())->get();

        foreach ($listeners as $listener) {
            if ($event->isPropagationStopped()) {
                break;
            }

            $returnValue = call_user_func($listener, $event);

            if ($returnValue !== null) {
                $event->setValue($returnValue);
            }
        }

        return $event->getValue();
    }

    /**
     * @codeCoverageIgnore
     */
    public function __debugInfo()
    {
        return $this->events;
    }
}
