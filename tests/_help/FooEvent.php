<?php

use Avris\Dispatcher\Event;

class FooEvent extends Event
{
    /** @var string */
    private $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function getName(): string
    {
        return 'foo';
    }

    public function setValue($value): Event
    {
        $this->value = (string) $value;

        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }
}