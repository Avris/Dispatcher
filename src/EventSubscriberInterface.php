<?php
namespace Avris\Dispatcher;

interface EventSubscriberInterface
{
    public function getSubscribedEvents(): iterable;
}
