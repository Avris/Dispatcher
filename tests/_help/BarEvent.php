<?php

use Avris\Dispatcher\Event;

class BarEvent extends Event
{
    /** @var string */
    private $value;

    public function __construct(string $value)
    {
        $this->value = strtoupper($value);
    }

    public function getName(): string
    {
        return 'bar';
    }

    public function setValue($value): Event
    {
        $this->value = strtoupper($value);

        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }
}