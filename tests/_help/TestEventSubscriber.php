<?php

use Avris\Dispatcher\EventSubscriberInterface;

class TestEventSubscriber implements EventSubscriberInterface
{
    public function getSubscribedEvents(): iterable
    {
        yield 'foo' => function (FooEvent $e) {
            return $e->getValue() . ' notImportant';
        };

        yield 'foo:9' => function (FooEvent $e) {
            return $e->getValue() . ' IMPORTANT';
        };

        yield 'bar' => function (BarEvent $e) {
            $e->stopPropagation();

            return $e->getValue() . ' stopping';
        };

        yield 'bar' => function (BarEvent $e) {
            return $e->getValue() . ' notReached';
        };

        yield 'nonexistent' => function () {
            throw new \Exception('Unexpected');
        };
    }
}
