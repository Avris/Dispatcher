<?php
namespace Avris\Dispatcher;

use PHPUnit\Framework\TestCase;

class EventDispatcherTest extends TestCase
{
    /** @var EventDispatcher */
    private $dispatcher;

    protected function setUp()
    {
        $this->dispatcher = new EventDispatcher();
    }

    public function testDispatcher()
    {
        $this->dispatcher->registerSubscriber(new \TestEventSubscriber());
        $this->dispatcher->attachListener('foo', function (\FooEvent $e) {
            return $e->getValue() . ' VIL';
        }, 999);

        $fooEvent = new \FooEvent('start');
        $result = $this->dispatcher->trigger($fooEvent);
        $this->assertEquals('start VIL IMPORTANT notImportant', $result);
        $this->assertEquals('start VIL IMPORTANT notImportant', $fooEvent->getValue());

        $barEvent = new \BarEvent('start');
        $result = $this->dispatcher->trigger($barEvent);
        $this->assertEquals('START STOPPING', $result);
        $this->assertEquals('START STOPPING', $barEvent->getValue());

        $this->dispatcher->clear('foo');
        $this->assertEquals('foo', $this->dispatcher->trigger(new \FooEvent('foo')));
        $this->assertEquals('BAR STOPPING', $this->dispatcher->trigger(new \BarEvent('bar')));
    }
}
