<?php
namespace Avris\Dispatcher;

interface EventDispatcherInterface
{
    public function registerSubscriber(EventSubscriberInterface $subscriber): EventDispatcherInterface;

    public function attachListener(string $event, callable $listener, int $priority = 0): self;

    public function clear(string $event): self;

    public function trigger(Event $event);
}
